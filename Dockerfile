FROM multiarch/alpine:_DISTCROSS-edge
MAINTAINER Adrien le Maire <adrien@alemaire.be>
COPY docker-entrypoint.sh /usr/local/bin/
ARG MARIADB_VERSION
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories && \
    apk add --update mariadb=$MARIADB_VERSION bash gosu  && \
    mkdir -p /run/mysqld && \
    chmod +x usr/local/bin/docker-entrypoint.sh && \
    ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]
EXPOSE 3306/tcp
VOLUME ["/var/lib/mysql"]
CMD ["mysqld"]
